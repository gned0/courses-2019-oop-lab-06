package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4885265031519443556L;

	public String getMessage() {
        return "Unable to complete operation: Insufficient funds";
    }
}
