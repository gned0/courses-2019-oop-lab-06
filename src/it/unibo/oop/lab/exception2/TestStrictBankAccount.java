package it.unibo.oop.lab.exception2;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	final AccountHolder usr1 = new AccountHolder("Mario", "Rossi", 1);
        final AccountHolder usr2 = new AccountHolder("Luigi", "Bianchi", 2);
        final StrictBankAccount account1 = new StrictBankAccount(usr1.getUserID(), 10_000, 10);
        final StrictBankAccount account2 = new StrictBankAccount(usr2.getUserID(), 10_000, 10);
       /* 
        * My very own test
        try {
        	account1.deposit(3, 10);
        	fail();
        } catch (WrongAccountHolderException e) {
        	assertNotNull(e);
        }
        
        try {
        	for(int i = 1; i<12; i++) {
            	account1.withdrawFromATM(1, 1);
            }
        	fail();
        } catch (TransactionsOverQuotaException e) {
        	assertNotNull(e);
        }
        
        try {
        	account1.withdraw(1, 100_000);
        	fail();
        } catch (NotEnoughFoundsException e) {
        	assertNotNull(e);
        }
        */
        
        //Pianini's test
        try {
            account1.deposit(4, 100);
            fail();
        } catch (WrongAccountHolderException e) {
            assertNotNull(e);
        }
        for (int i = 1; i <= 10; i++) {
            try {
                account2.depositFromATM(usr2.getUserID(), 1);
                System.out.println(account2.getNTransactions());
            } catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
                fail("Not exceeded yet max no. transactions!");
            }
        }
        /*
         * Questa istruzione genererà una eccezione di tipo
         * TransactionsOverQuotaException
         */
        try {
            account2.depositFromATM(usr2.getUserID(), 1);
            fail("Should raise the exception signaling that we exceeded max no. transactions!");
        } catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
            assertNotNull(e);
        }
        try {
            /*
             * Questa istruzione genererà una eccezione di tipo
             * NotEnoughFoundsException
             */
            account1.withdraw(usr1.getUserID(), 100_000_000);
        } catch (WrongAccountHolderException e) {
            fail();
        } catch (NotEnoughFoundsException e) {
            assertNotNull(e);
        }
    }
}
