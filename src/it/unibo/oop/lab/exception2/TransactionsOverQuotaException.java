package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends IllegalStateException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6467343363892915497L;

	public String getMessage() {
        return "Unable to complete operation: Quota exceeded";
    }
}
