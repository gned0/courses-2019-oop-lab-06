package it.unibo.oop.lab.collections1;


import java.util.*;
import java.util.Map.Entry;


/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    	
    	final int N = 100_000;
    	final int TO_MS = 1_000_000;
    	
    	List<Integer> numbers = new ArrayList<Integer>(); 
    	for(int i = 1000; i<2000; i++) {
    		numbers.add(i);
    	}
    	LinkedList<Integer> linkednumbers = new LinkedList<Integer>();
    	linkednumbers.addAll(numbers);
    	
    	int i = numbers.get(0);
    	numbers.set(0, numbers.get(numbers.size()-1));
    	numbers.set(numbers.size() - 1, i);
    	
    	for(int n : numbers) {
    		System.out.println(n);
    	}
    	
    	
        long time = System.nanoTime();
        
        for(int counter = 1; counter <= N; counter++) {
        	numbers.add(0, counter);
        }
        
        time = System.nanoTime() - time;
        
        System.out.println("Adding " + N
                + " integers in an ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        
        
        long time1 = System.nanoTime();
        
        for(int counter = 1; counter <= N; counter++) {
        	linkednumbers.addFirst(counter);
        }
        
        time1 = System.nanoTime() - time1;
        
        System.out.println("Adding " + N
                + " integers in a LinkedList took " + time1
                + "ns (" + time1 / TO_MS + "ms)");
        
        
        long time2 = System.nanoTime();
        
        for(int counter = 1; counter <= N; counter++) {
        	numbers.get(numbers.size()/2);
        }
        
        time2 = System.nanoTime() - time2;
        
        System.out.println("Reading " + N
                + " integers in an ArrayList took " + time2
                + "ns (" + time2 / TO_MS + "ms)");
        
        
        
        long time3 = System.nanoTime();
        
        for(int counter = 1; counter <= N; counter++) {
        	linkednumbers.get(numbers.size()/2);
        }
        
        time3 = System.nanoTime() - time3;
        
        System.out.println("Reading " + N
                + " integers in a LinkedList took " + time3
                + "ns (" + time3 / TO_MS + "ms)");
        
        Map<String, Long> world = new HashMap<String, Long>();
        
        world.put("Africa", (long) 1_110_636_000);
        world.put("Americas", (long) 972_005_000);
        world.put("Antarctica", (long) 0);
        world.put("Asia",  (long) 1_298_723_000);
        world.put("Europe", (long) 742_452_000);
        world.put("Oceania", (long) 38_304_000);
        
        long total = 0;
        for (Entry<String, Long> entry : world.entrySet()) {
		   total = total + entry.getValue();
		}
        System.out.println(total);
    }
    
    
    /*
    Africa -> 1,110,635,000;
    
    * Americas -> 972,005,000
    * 
    * Antarctica -> 0
    * 
    * Asia -> 4,298,723,000
    * 
    * Europe -> 742,452,000
    * 
    * Oceania -> 38,304,000
    * */
}

